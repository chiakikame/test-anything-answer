const test = require('tape');
const fancify = require(process.argv[2]);

test('fancify in default setting', (t) => {
  t.equal(fancify('gg'), '~*~gg~*~');
  t.end();
});

test('fancify with capping switch', (t) => {
  t.equal(fancify('gg', false), '~*~gg~*~');
  t.equal(fancify('gg', true), '~*~GG~*~');
  t.end();
});

test('fancify, full 3 options', (t) => {
  t.equal(fancify('gg', false), '~*~gg~*~');
  t.equal(fancify('gg', true), '~*~GG~*~');
  t.equal(fancify('gg', false, 'v'), '~v~gg~v~');
  t.equal(fancify('gg', true, '#$'), '~#$~GG~#$~');
  t.end();
});
