const test = require('tape');
const rCb = require(process.argv[2]);
const callCount = Math.floor(Math.random() * 20) + 1;

test('repeatedCallback test', (t) => {
  t.plan(callCount);
  rCb(callCount, () => {
    t.pass("Callback called");
  });
});
