// A function feedCat takes any kind of food as a String argument and returns
// 'yum' for everything you feed them. However if you try to feed the cat
// 'chocolate', the function will throw an error.
//
// Write tests for feedCat to be sure kittens can be fed yummy food without
// being harmed.
const test = require('tape');
const feedCat = require(process.argv[2]);

const foods = ['die Apfelsaft', 'der Hund', 'der Mann'];

test('feedCat', (t) => {
  foods.forEach(f => {
    t.equal(feedCat(f), 'yum');
  });
  
  t.throws(() => feedCat('chocolate'));
  
  t.end();
});
